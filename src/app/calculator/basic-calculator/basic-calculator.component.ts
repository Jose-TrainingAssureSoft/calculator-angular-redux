import { Component, OnInit } from '@angular/core';
import { calculateOperation,clear } from '../../actions/counter.actions';
import { Store, props } from '@ngrx/store';
import { State } from 'src/app/reducers';

@Component({
  selector: 'app-basic-calculator',
  templateUrl: './basic-calculator.component.html',
  styleUrls: ['./basic-calculator.component.css']
})
export class BasicCalculatorComponent implements OnInit {
  input: string = '';
  result$: any;

  constructor(
    private store:Store<State>
  ) {
    this.result$=this.store.select('counter');
  }

  ngOnInit(): void {
  }


  pressNum(num: string) {

    if (num == "0") {
      if (this.input == "") {
        return;
      }
      const PrevKey = this.input[this.input.length - 1];
      if (PrevKey === '/' || PrevKey === '*' || PrevKey === '-' || PrevKey === '+') {
        return;
      }
    }

    this.input = this.input + num
    this.calcAnswer();
  }



  pressOperator(op: string) {

    const lastKey = this.input[this.input.length - 1];
    if (lastKey === '/' || lastKey === '*' || lastKey === '-' || lastKey === '+') {
      return;
    }
    this.input = this.input + op
  }


  clear() {
    if (this.input != "") {
      this.input = this.input.substr(0, this.input.length - 1)
    }
  }

  allClear() {
    this.store.dispatch(clear());
    console.log(this.result$)
    this.input = '';
  }

  calcAnswer() {
    this.result$=this.store.select('counter');
    let formula = this.input;

    let lastKey = formula[formula.length - 1];

    if (lastKey === '.') {
      formula = formula.substr(0, formula.length - 1);
    }

    lastKey = formula[formula.length - 1];

    if (lastKey === '/' || lastKey === '*' || lastKey === '-' || lastKey === '+' || lastKey === '.') {
      formula = formula.substr(0, formula.length - 1);
    }

    console.log("Formula: " + formula);

    // this.result = eval(formula);
    this.store.dispatch(calculateOperation({formula:formula}));
  }

  getAnswer() {
    this.calcAnswer();
    this.input =  this.result$.actionsObserver.value.formula ;
    if (this.input == "0") this.input = "";
  }

}
