import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { reducer } from './counter.reducer';


export interface State {
counter:number;
}

export const reducers: ActionReducerMap<State> = {
counter:reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
