import { Action, createReducer, on } from '@ngrx/store';
import { calculateOperation,clear } from '../actions/counter.actions';


export const counterFeatureKey = 'counter';


export const initialState: number = 0//decrementCounters.arguments('numberA');

export const reducer = createReducer(
  initialState,
  on(calculateOperation, (state,{formula})=>eval(formula)),
  on(clear, state=>state =0)

);

