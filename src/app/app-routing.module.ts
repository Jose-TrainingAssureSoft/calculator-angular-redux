import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    // path:'counter',
    // loadChildren:()=>import('./counter/counter.module').then(m=>m.CounterModule)
    path:'calculator',
    loadChildren:()=>import('./calculator/calculator.module').then(m=>m.CalculatorModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
