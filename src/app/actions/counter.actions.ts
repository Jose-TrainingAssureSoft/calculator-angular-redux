import { createAction, props } from '@ngrx/store';

export const increaseCounters = createAction(
  '[Counter] Increase Counters'
);


export const calculateOperation = createAction(
  '[Calculate] CalculateOperation',
  props<{ formula: string}>()
);

export const clear = createAction(
  '[Calculate] clear',
);




