import { Component, OnInit } from '@angular/core';
import { Store, props } from '@ngrx/store';
import { State } from 'src/app/reducers';
import { ClickCounterService } from './click-counter.service';
import { calculateOperation } from '../../actions/counter.actions';

@Component({
  selector: 'app-click-counter',
  templateUrl: './click-counter.component.html',
  styleUrls: ['./click-counter.component.css']
})
export class ClickCounterComponent implements OnInit {

  public number:number=5;
  public count$:any;

  constructor(
    private clickCounterService:ClickCounterService,
    private store:Store<State>
  ) {
    // this.count=this.clickCounterService.getCount();
    this.count$=this.store.select('counter');
   }

  ngOnInit(): void {
  }

  public onClick(){
    // this.clickCounterService.increaseCount();
    // this.count=this.clickCounterService.getCount();
    // this.store.dispatch(add({A:this.number,B:this.number}));


  }

}
