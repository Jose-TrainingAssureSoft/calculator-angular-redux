import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClickCounterService {

  public count=0;

  constructor() { }

  public increaseCount(){
    this.count++;
  }

  public getCount(){
    return this.count;
  }
}
