import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickCounterComponent } from './click-counter/click-counter.component';
import { Routes, RouterModule } from '@angular/router';

const routes:Routes=[
  {
  path:'',
  component:ClickCounterComponent
  }
];


@NgModule({
  declarations: [
    ClickCounterComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CounterModule { }
